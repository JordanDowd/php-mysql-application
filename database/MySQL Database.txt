-- create and select the database
DROP DATABASE IF EXISTS battlefield_One_Weapons;
CREATE DATABASE battlefield_One_Weapons;
USE battlefield_One_Weapons;  -- MySQL command

-- create the tables
CREATE TABLE categories (
  categoryID      INT(11)        NOT NULL   AUTO_INCREMENT,
  categoryName     VARCHAR(255)   NOT NULL,
  description	VARCHAR(1000),
  PRIMARY KEY (categoryID)
);

CREATE TABLE weapons (
  weaponID        INT(11)        NOT NULL   AUTO_INCREMENT,
  categoryID      INT(11)        NOT NULL,
  weaponName      VARCHAR(255)   NOT NULL,
  damage     INT(11)        NOT NULL,
  accuracy     INT(11)        NOT NULL,
  rof     INT(11)        NOT NULL,
  ammo     INT(11)        NOT NULL,
  PRIMARY KEY (weaponID),
  FOREIGN KEY (categoryID) REFERENCES categories(categoryID)
);


-- insert data into the database
INSERT INTO categories VALUES
(1, 'Rifles', 'Rifles in Battlefield One do incredible damage at medium to close range with a very exceptable rate of fire. Beware of the reload, as it takes quite some time.'),
(2, 'Snipers', 'Snipers are the go to weapon for long range engagements. Snipers have the best damage and accuracy out of all the classes. Snipers are one shot kill to the head. So if your getting surrounded, go for them headshots!'),
(3, 'SMG', 'SMGs do high damage during close range engagements with a high range of fire and quick reload speeds. Dont run out into the open areas while using this weapon as you are more than likely going to die from better medium and long range weapons. keep low and in narrow areas and youll destroy the enemy.'),
(4, 'LMG', 'LMGs are the go to weapon to fire heeps of bullets down range, suppressing your enemy. With your large magazine and medium rate of fire, youll never have to worry about reloading in tight situations.'),
(5, 'Shotguns', 'Shotguns are close range beasts, get cuaght in a close space with an enemy shotgunner and your 99% going to lose. What is has for incredible damage, it lacks in range so keep in close quaters and youll be fine.'),
(6, 'Pistols', 'Pistols are especially handy when you run out of ammo of your primary and you need that bullet to finish off an enemy, pistols do just that. With a decent range, pistol can do quite some damage.'),
(7, 'Melee Weapons', 'Melee weapons are deadly in the battlefield with most being a one hit kill. Melee weapons range from high damage, slow strike rate to low damage, high strike rate so choose wisely.');

INSERT INTO weapons VALUES
(1, 1, 'CEI Rigotti', 43, 48, 300, 10),
(2, 1, 'SELBSTLADER m1916', 54, 56, 225, 26),
(3, 1, 'Mondragon M1908', 54, 56, 257, 10),
(4, 1, 'M1907 SL', 37, 48, 300, 21),
(5, 2, 'SMLE MKIII', 100, 100, 53, 10),
(6, 2, 'Gewehr 98', 100, 100, 50, 5),
(7, 2, 'Gewehr M.95', 75, 100, 67, 5),
(8, 2, 'Russian 1895', 100, 100, 56, 5),
(9, 2, 'M1903', 100, 100, 51, 5),
(10, 3, 'MP18', 23, 25, 550, 32),
(11, 3, 'Automatico M1918', 21, 22, 900, 25),
(12, 3, 'Villar Perosa', 22, 30, 3000, 50),
(13, 4, 'Lewis Gun', 24, 35, 480, 47),
(14, 4, 'MG 15', 27, 33, 500, 100),
(15, 4, 'Benet Mercier M1909', 27, 45, 450, 30),
(16, 4, 'Madsen MG', 27, 28, 540, 31),
(17, 4, 'Hellriegel 1915', 23, 26, 650, 60),
(18, 5, 'M97 Trench', 62, 25, 138, 5),
(19, 5, 'Model 10-A Slug', 88, 33, 78, 6),
(20, 6, 'Colt M1911', 44, 25, 300, 8),
(21, 6, 'Mauser C96', 28, 32, 299, 10),
(22, 6, 'Luger P08', 20, 38, 250, 10),
(23, 6, 'Modello 1915', 21, 35, 200, 8),
(24, 6, 'Auto Revolver', 36, 24, 100, 6),
(25, 7, 'Bayonet', 100, 0, 0, 0),
(26, 7, 'Bolo Knife', 20, 0, 0, 0),
(27, 7, 'Shovel', 50, 0, 0, 0);




-- create the users and grant priveleges to those users
GRANT SELECT, INSERT, DELETE, UPDATE
ON battlefield_One_Weapons.*
TO mgs_user@localhost
IDENTIFIED BY 'pa55word';

GRANT SELECT
ON weapons
TO mgs_tester@localhost
IDENTIFIED BY 'pa55word';