<?php
$dsn = 'mysql:host=localhost;dbname=battlefield_One_Weapons';
$username = 'mgs_user';
$password = 'pa55word';

try {
    $db = new PDO($dsn, $username, $password);
} catch (Exception $e) {
    $error_message = $e->getMessage();
    include('database_error.php');
    exit();
}

?>
