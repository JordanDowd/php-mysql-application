<!DOCTYPE html>
<html>
    <!-- the head section -->
    <head>
        <title>My Guitar Shop</title>
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/categoryMenu.css" />
        <link rel="stylesheet" type="text/css" href="css/background.css" />
        <link rel="stylesheet" type="text/css" href="css/menu_bar.css" />

    </head>
    <!-- the body section -->
    <body>
        <!-- Battle Field 1 background -->
        <div class="background"></div> 
        <div class="fog_one"></div>
        <div class="fog_two"></div>

        <!-- Main -->
        
        <header><h1>Battlefield One Weapons</h1></header>
        <?php include'inc/menu.php' ?>
        <main>   
            <h1>Welcome to Battlefield One: Weapons List</h1>
            <p>Similar to previous entries in the series, 
                Battlefield 1 is a first-person shooter that emphasizes teamwork. 
                The game is set in the period of World War I, and is inspired by historic events.
                <br></br> 
                Players can make use of World War I weapons, including bolt-action rifles, automatic 
                and semi-automatic rifles, artillery, flamethrowers, and mustard gas to combat opponents.
                <br></br> 
                Melee combat was reworked, with DICE introducing new melee weapons such as sabres, trench clubs, 
                and shovels into the game. These melee weapons were divided into two groups: heavy and light.
            </p>
        </main>
        <?php include 'inc/footer.php'; ?>
    </body>
</html>
