<?php
require_once('database.php');

//Get category ID
if (!isset($category_id)) {
    $category_id = filter_input(INPUT_GET, 'category_id', FILTER_VALIDATE_INT);
    if ($category_id == NULL || $category_id == FALSE) {
        $category_id = 1;
    }
}

//Get name for selected category
$queryCategory = 'SELECT * FROM categories
        WHERE categoryID = :category_id';
$statement1 = $db->prepare($queryCategory);
$statement1->bindValue(':category_id', $category_id);
$statement1->execute();
$category = $statement1->fetch();
$category_name = $category['categoryName'];
$statement1->closeCursor();

//Get all categories
$query = 'SELECT * FROM categories ORDER BY categoryID';
$statement = $db->prepare($query);
$statement->execute();
$categories = $statement->fetchAll();
$statement->closeCursor();

//Get products for selected category
$queryWeapons = 'SELECT * FROM weapons WHERE categoryID = :category_id ORDER BY weaponID';
$statement3 = $db->prepare($queryWeapons);
$statement3->bindValue(':category_id', $category_id);
$statement3->execute();
$weapons = $statement3->fetchAll();
$statement3->closeCursor();

//get Category Details
$queryDetails = 'SELECT categoryName, description FROM categories WHERE categoryID = :category_id ORDER BY categoryID';
$statement4 = $db->prepare($queryDetails);
$statement4->bindValue(':category_id', $category_id);
$statement4->execute();
$details = $statement4->fetchAll();
$statement4->closeCursor();
?>
<!DOCTYPE html>
<html>
    <!-- the head section -->
    <head>
        <title>My Guitar Shop</title>
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/categoryMenu.css" />
        <link rel="stylesheet" type="text/css" href="css/background.css" />
        <link rel="stylesheet" type="text/css" href="css/menu_bar.css" />
        <link href="https://fonts.googleapis.com/css?family=Oswald|PT+Sans|Ubuntu|Yanone+Kaffeesatz" rel="stylesheet">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <script>
            //source: http://www.w3schools.com/php/php_ajax_database.asp
            $(document).ready(function () {
                $("a.x").click(function () {
                    var str = $(this).attr('href');
                    $('a.x').on('click', showWeapons(str));
                    return false;
                });
            });

            function showWeapons(str) {
                if (str === "") {
                    document.getElementById("weapon_list").innerHTML = "";
                    return;
                } else {
                    if (window.XMLHttpRequest) {
                        // code for IE7+, Firefox, Chrome, Opera, Safari
                        xmlhttp = new XMLHttpRequest();
                    } else {
                        // code for IE6, IE5
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function () {
                        if (this.readyState === 4 && this.status === 200) {
                            document.getElementById("weapon_list").innerHTML = this.responseText;
                        }
                    };
                    xmlhttp.open("GET", "show_weapons.php?category_id=" + str, true);
                    xmlhttp.send();
                }
            }
        </script>
    </head>
    <!-- the body section -->
    <body>
        <!-- Battle Field 1 background -->
        <div class="background">
            <div class="fog_one"></div>
            <div class="fog_two"></div>
        </div> 
        <!-- Main -->

        <header><h1>BATTLEFIELD 1 WEAPONS</h1></header>
        <main>
            <aside>
                <?php include 'inc/menu.php'; ?>
                <!-- display a list of categories -->
                <h2>Categories</h2>
                <?php include 'inc/category_menu.php'; ?>
                <div id="weapon_list">
                    <div id="desc"> 
                        <h1> Select a category from above </h1>
                        <p> Selecting a category will show you the weapons below associated with that particular category. 
                            Average of each weapon stat for that category will be displayed here. </p>
                    </div>
                </div>
            </aside>
            <section id="weapon_list"></section>

        </main>
        <?php include 'inc/footer.php'; ?>
    </body>
</html>

