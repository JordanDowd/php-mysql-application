# PHP/mySQL Application

This was a website i created for my 2nd year web development project that utilized mySQL and PHP.

The website allows you to filter through each weapon type and the signed in user can update, edit and delete weapons from the website.
This website also utilizes ajax to update the filtered weapons without refreshing the page.

The database can be created using the MySQL Database.txt located in the database folder.