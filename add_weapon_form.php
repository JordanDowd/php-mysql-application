<?php
require('database.php');
$query = 'SELECT * FROM categories ORDER BY categoryID';
$statement = $db->prepare($query);
$statement->execute();
$categories = $statement->fetchAll();
$statement->closeCursor();
?>
<!DOCTYPE html>
<html class="add_products">

    <!-- the head section -->
    <head>
        <title>Battlefield One</title>
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/categoryMenu.css" />
        <link rel="stylesheet" type="text/css" href="css/background.css" />
        <link rel="stylesheet" type="text/css" href="css/menu_bar.css" />
    </head>

    <!-- the body section -->
    <body class="add">
        <header><h1>BATTLEFIELD 1 WEAPONS</h1></header>

        <main>
            <h1>Add Weapon</h1>
            <form action="add_weapon.php" method="post" id="add_product_form">

                <label>Category:</label>
                <select name="category_id">
                    <?php foreach ($categories as $category) : ?>
                        <option value="<?php echo $category['categoryID']; ?>">
                            <?php echo $category['categoryName']; ?>
                        </option>
                    <?php endforeach; ?>
                </select><br>

                <label>Name:</label>
                <input type="text" name="weapon_name"><br>

                <label>Damage:</label>
                <input type="text" name="damage"><br>

                <label>Accuracy:</label>
                <input type="text" name="accuracy"><br>

                <label>Rate of Fire:</label>
                <input type="text" name="rate_of_fire"><br>

                <label>Ammo:</label>
                <input type="text" name="ammo"><br>

                <label>&nbsp;</label>
                <button class="addButton" type="submit"><span class="addspan">Add Weapon</span></button>
            </form>
            <a class="extra" type="submit" href="index.php" ><span class="extraspan">Cancel</span></a>
            
        </main>

        <?php include 'inc/footer.php'; ?>
    </body>
</html>