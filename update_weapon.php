<?php
// Get the product data
$category_id = filter_input(INPUT_POST, 'category_id', FILTER_VALIDATE_INT);
$weapon_id = filter_input(INPUT_POST, 'weapon_id', FILTER_VALIDATE_INT);
$weapon_name = filter_input(INPUT_POST, 'weapon_name');
$damage = filter_input(INPUT_POST, 'damage', FILTER_VALIDATE_INT);
$accuracy = filter_input(INPUT_POST, 'accuracy', FILTER_VALIDATE_INT);
$rate_of_fire = filter_input(INPUT_POST, 'rate_of_fire', FILTER_VALIDATE_INT);
$ammo = filter_input(INPUT_POST, 'ammo', FILTER_VALIDATE_INT);

// Validate inputs
if ($category_id == null || $category_id == false 
        || $weapon_id == null || $weapon_id == false
        || $weapon_name == null 
        || $damage == null || $damage == false
        || $accuracy == null || $accuracy == false
        || $rate_of_fire == null || $rate_of_fire == false
        || $ammo == null || $ammo == false) {
    $error = "Invalid product data. Check all fields and try again.";
    include('database_error.php');
} else {
    require_once('database.php');

    // Add the product to the database 
    $query = 'UPDATE weapons 
        SET categoryID = :category_id, 
         weaponName = :weapon_name,
         damage = :damage,
         accuracy = :accuracy,
         rof = :rate_of_fire,
         ammo = :ammo WHERE weaponID = :weapon_id';
         
    $statement = $db->prepare($query);
    $statement->bindValue(':category_id', $category_id);
    $statement->bindValue(':weapon_id', $weapon_id);
    $statement->bindValue(':weapon_name', $weapon_name);
    $statement->bindValue(':damage', $damage);
    $statement->bindValue(':accuracy', $accuracy);
    $statement->bindValue(':rate_of_fire', $rate_of_fire);
    $statement->bindValue(':ammo', $ammo);
    $statement->execute();
    $statement->closeCursor();

    // Display the Product List page
    include('index.php');
}

