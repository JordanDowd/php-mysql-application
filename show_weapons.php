<?php
require_once('database.php');

//Get category ID
if (!isset($category_id)) {
    $category_id = filter_input(INPUT_GET, 'category_id', FILTER_VALIDATE_INT);
    if ($category_id == NULL || $category_id == FALSE) {
        $category_id = 1;
    }
}

//Get name for selected category
$queryCategory = 'SELECT * FROM categories
        WHERE categoryID = :category_id';
$statement1 = $db->prepare($queryCategory);
$statement1->bindValue(':category_id', $category_id);
$statement1->execute();
$category = $statement1->fetch();
$category_name = $category['categoryName'];
$statement1->closeCursor();

//Get all categories
$query = 'SELECT * FROM categories ORDER BY categoryID';
$statement = $db->prepare($query);
$statement->execute();
$categories = $statement->fetchAll();
$statement->closeCursor();

//Get products for selected category
$queryWeapons = 'SELECT * FROM weapons WHERE categoryID = :category_id ORDER BY weaponID';
$statement3 = $db->prepare($queryWeapons);
$statement3->bindValue(':category_id', $category_id);
$statement3->execute();
$weapons = $statement3->fetchAll();
$statement3->closeCursor();

//get Category Details
$queryDetails = 'SELECT categoryName, description FROM categories WHERE categoryID = :category_id ORDER BY categoryID';
$statement4 = $db->prepare($queryDetails);
$statement4->bindValue(':category_id', $category_id);
$statement4->execute();
$details = $statement4->fetchAll();
$statement4->closeCursor();

//get Average Stats
$sql = 'SELECT ROUND(AVG(damage), 0) AS Damage, ROUND(AVG(accuracy), 0) AS Accuracy, '
        . 'ROUND(AVG(rof), 0) AS rateOfFire, ROUND(AVG(ammo), 0) AS Ammo FROM weapons WHERE categoryID = :category_id';
$stat = $db->prepare($sql);
$stat->bindValue(':category_id', $category_id);
$stat->execute();
$avg = $stat->fetchAll();
$stat->closeCursor();
?>
<div id="desc">
    <?php foreach ($details as $n) : ?>
        <h1> <?php echo $n['categoryName']; ?> </h1>
        <p> <?php echo $n['description']; ?> </p>
        <h1 class="avgh1"> <?php echo $n['categoryName']; ?>'s Average Statistics </h1>
    <?php endforeach; ?>
    <table class="avg">
        <tr>
            <th>Damage</th>
            <th>Accuracy</th>
            <th>Rate of Fire</th>
            <th class="right">Ammo</th>
        </tr>
        <div>
            <tr>
                <?php foreach ($avg as $n) : ?>
                <td value="0"><?php echo $n['Damage']; ?></td>
                <td value="0"><?php echo $n['Accuracy']; ?></td>
                <td value="0"><?php echo $n['rateOfFire']; ?></td>
                <td class="right" value="0"><?php echo $n['Ammo']; ?></td>
                <?php endforeach; ?>
            </tr>
        </div>
    </table>
</div>
<?php foreach ($weapons as $weapon) : ?>
    <section class="content" >
        <!-- display a table of products -->
        <h2 class="weaponName"><?php echo $weapon['weaponName']; ?></h2>
        <table>
            <tr>
                <th>Damage</th>
                <th>Accuracy</th>
                <th>Rate of Fire</th>
                <th class="right">Ammo</th>
            </tr>
            <div>
                <tr>
                    <td><?php echo $weapon['damage']; ?></td>
                    <td><?php echo $weapon['accuracy']; ?></td>
                    <td><?php echo $weapon['rof']; ?></td>
                    <td class="right"><?php echo $weapon['ammo']; ?></td>  
                </tr>
            </div>
        </table>
        <form action="delete_weapon.php" method="post">
            <input type="hidden" name="weapon_id" value="<?php echo $weapon['weaponID']; ?>">
            <input type="hidden" name="category_id" value="<?php echo $weapon['categoryID']; ?>">
            <button class="button" type="submit"><span>Delete</span></button>
        </form>
        <form action="update_weapon_form.php?cid=<?php echo $weapon['categoryID']; ?>&wid=<?php echo $weapon['weaponID']; ?>" method="post">
            <button class="update" type="submit"><span class="uspan">Update</span></button>
        </form>
    </section>
<?php endforeach; ?>


