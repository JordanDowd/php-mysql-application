<?php
require('database.php');

$category_id = $_GET['cid'];
$weapon_id = $_GET['wid'];

$query = 'SELECT * FROM categories ORDER BY categoryID';
$statement = $db->prepare($query);
$statement->execute();
$categories = $statement->fetchAll();
$statement->closeCursor();

$weaponCategory = 'SELECT * FROM categories WHERE categoryID = :category_id';
$statement3 = $db->prepare($weaponCategory);
$statement3->bindValue(':category_id', $category_id);
$statement3->execute();
$category = $statement3->fetchAll();
$statement->closeCursor();

$sql = 'SELECT * FROM weapons WHERE weaponID = :weapon_id';
$statement2 = $db->prepare($sql);
$statement2->bindValue(':weapon_id', $weapon_id);
$statement2->execute();
$weapon = $statement2->fetchAll();
$statement->closeCursor();
?>
<!DOCTYPE html>
<html class="add_products">

    <!-- the head section -->
    <head>
        <title>Battlefield One</title>
        <link rel="stylesheet" type="text/css" href="css/main.css" />
        <link rel="stylesheet" type="text/css" href="css/categoryMenu.css" />
        <link rel="stylesheet" type="text/css" href="css/background.css" />
        <link rel="stylesheet" type="text/css" href="css/menu_bar.css" />
    </head>

    <!-- the body section -->
    <body class="add">
        <header><h1>BATTLEFIELD 1 WEAPONS</h1></header>

        <main>
            <h1>Add Weapon</h1>
            <form action="update_weapon.php" method="post" id="add_product_form">
                
                <?php foreach ($weapon as $update) : ?>
                <label>Category:</label>
                <select name="category_id">
                    <?php foreach ($category as $cat) : ?>
                        <option value="<?php echo $cat['categoryID']; ?>">
                            <?php echo $cat['categoryName']; ?>
                        </option>
                    <?php endforeach; ?>
                    <?php foreach ($categories as $category) : ?>
                        <option value="<?php echo $category['categoryID']; ?>">
                            <?php echo $category['categoryName']; ?>
                        </option>
                    <?php endforeach; ?>
                </select><br>
                
                <input type="hidden" name="weapon_id" value="<?php echo $update['weaponID']; ?>" required>
                
                <label>Name:</label>
                <input type="text" name="weapon_name" value="<?php echo $update['weaponName']; ?>"><br>

                <label>Damage:</label>
                <input type="text" name="damage" value="<?php echo $update['damage']; ?>"><br>

                <label>Accuracy:</label>
                <input type="text" name="accuracy" value="<?php echo $update['accuracy']; ?>"><br>

                <label>Rate of Fire:</label>
                <input type="text" name="rate_of_fire" value="<?php echo $update['rof']; ?>"><br>

                <label>Ammo:</label>
                <input type="text" name="ammo" value="<?php echo $update['ammo']; ?>"><br>
                <?php endforeach; ?>

                <label>&nbsp;</label>
                <button class="addButton" type="submit"><span class="addspan">Update</span></button>
            </form>
            <a class="extra" type="submit" href="index.php" ><span class="extraspan">Cancel</span></a>
        </main>

        <?php include 'inc/footer.php'; ?>
    </body>
</html>
