<?php
require ('database.php');

$weapon_id = filter_input(INPUT_POST, 'weapon_id', FILTER_VALIDATE_INT);
$category_id = filter_input(INPUT_POST, 'category_id', FILTER_VALIDATE_INT);

//Delete the product from the database
if ($weapon_id != FALSE && $category_id != FALSE){
    $query = 'DELETE FROM weapons WHERE weaponID = :weapon_id';
    $statement = $db->prepare($query);
    $statement->bindValue(':weapon_id', $weapon_id);
    $success = $statement->execute();
    $statement->closeCursor();
}

// Display the product list page
include('index.php');
?>
